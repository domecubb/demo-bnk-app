import 'package:flutter/material.dart';

class NotifyScreen extends StatelessWidget {
  const NotifyScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              // Handle back button press
              Navigator.pop(context);
            },
          ),
          title: Text('แจ้งเตือน'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
              },
            ),
          ],
        ),
        body: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image:
                  AssetImage('images/lake.jpg'), 
              fit: BoxFit.cover, 
            ),
          ),
          alignment: Alignment.topCenter, 
          padding: EdgeInsets.all(16.0), 
          child: Card(
            elevation: 4.0,
            child: Container(
              child:  ListsWithCards()
              )
             ),
        ),
      ),
    );
  }
}



class ListsWithCards extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Sample data for three lists
    List<List<String>> listsData = [
      ['15 มี.ค. 57', 'เข้าสู่ระบบ'],
      ['15 มี.ค. 57', 'เข้าสู่ระบบ'],
      ['14 มี.ค. 57', 'เข้าสู่ระบบ'],
      ['13 มี.ค. 57', 'เข้าสู่ระบบ'],
      ['13 มี.ค. 57', 'เข้าสู่ระบบ'],     
      ['13 มี.ค. 57', 'เข้าสู่ระบบ'],
      ['13 มี.ค. 57', 'เข้าสู่ระบบ'],
      ['13 มี.ค. 57', 'เข้าสู่ระบบ'],     
      ['13 มี.ค. 57', 'เข้าสู่ระบบ'],
      ['13 มี.ค. 57', 'เข้าสู่ระบบ'],
      ['13 มี.ค. 57', 'เข้าสู่ระบบ'],     
      ['13 มี.ค. 57', 'เข้าสู่ระบบ'],
    ];

    return ListView.builder(
      itemCount: listsData.length,
      itemBuilder: (context, index) {
        return CardList(listData: listsData[index]);
      },
    );
  }
}

class CardList extends StatelessWidget {
  final List<String> listData;

  CardList({required this.listData});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(10.0),
      child: Column(
        children: [
          ListView.builder(
            itemCount: listData.length,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return ListTile(
                title: Text(listData[index]),
              );
            },
          ),
        ],
      ),
    );
  }
}