import 'package:flutter/material.dart';

class AccountScreen extends StatelessWidget {
  const AccountScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text('บัญชีของฉัน'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
              },
            ),
          ],
        ),
        body: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/lake.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          alignment: Alignment.topCenter,
          padding: EdgeInsets.all(16.0),
          child: Card(
            elevation: 4.0,
            child: Container(
              constraints: BoxConstraints(maxHeight: 250),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('บัญชีสินเชื่อ'),
                        TextButton(
                          onPressed: () {},
                          child: Text(
                            'ดูทั้งหมด',
                            style: TextStyle(
                              color: Color.fromARGB(255, 255, 72, 0),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 8.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('สมหมาย ทดสอบ'),
                      TextButton(
                        onPressed: () {},
                        child: Text(
                          'More',
                          style: TextStyle(
                            color: Color.fromARGB(255, 255, 72, 0),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 8.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('ยอดชำระ'),
                      TextButton(
                        onPressed: () {},
                        child: Text(
                          '0',
                          style: TextStyle(
                            color: Color.fromARGB(255, 255, 72, 0),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 8.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('ยอดเงินต้นคงเหลือ'),
                      TextButton(
                        onPressed: () {},
                        child: Text(
                          '0',
                          style: TextStyle(
                            color: Color.fromARGB(255, 255, 72, 0),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
