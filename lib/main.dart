import 'package:flutter/material.dart';
import 'account.dart';
import 'login.dart';
import 'notify.dart';
import 'setting.dart';

void main() => runApp(const BankApp());

class BankApp extends StatelessWidget {
  const BankApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (context) => const LoginScreen(),
        '/welcome': (context) => const WelcomeScreen(),
        '/account': (context) => const AccountScreen(),
      },
    );
  }
}

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
//  const String appTitle = 'Flutter layout demo';
    return const MaterialApp(
      home: Scaffold(
        backgroundColor: Color.fromARGB(255, 177, 177, 177),
        appBar: null,
        // #docregion addWidget
        body: SingleChildScrollView(
          child: Column(
            children: [
              ImageSection(
                image: 'images/lake.jpg',
              ),
              ButtonSection(),
              TextSection(
                description: 'Test',
              ),
            ],
          ),
        ),
        bottomNavigationBar: _DemoBottomAppBar(
          fabLocation: FloatingActionButtonLocation.endDocked,
          shape: CircularNotchedRectangle(),
        ),
      ),
    );
  }
}

class ButtonSection extends StatelessWidget {
  const ButtonSection({super.key});

  @override
  Widget build(BuildContext context) {
    final Color color = Theme.of(context).primaryColor;
    return const Center(child: Card1Section());
  }
}

class Card1Section extends StatelessWidget {
  const Card1Section({super.key});

  @override
  Widget build(BuildContext context) {
    final Color color = Theme.of(context).primaryColor;
    return Card(
      child: SizedBox(
        child: Column(
          children: [
            const SizedBox(
              height: 30.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                IconButtonWithLabel(
                  icon: Icons.star,
                  label: 'โอนเงิน',
                  onPressed: () {
                    Navigator.pushNamed(context, '/account');
                  },
                ),
                IconButtonWithLabel(
                  icon: Icons.favorite,
                  label: 'เติมเงิน',
                  onPressed: () {
                    // Add onPressed callback
                  },
                ),
                IconButtonWithLabel(
                  icon: Icons.thumb_up,
                  label: 'ชำระเงินกู้',
                  onPressed: () {
                    // Add onPressed callback
                  },
                ),
                IconButtonWithLabel(
                  icon: Icons.thumb_up,
                  label: 'สแกน',
                  onPressed: () {
                    // Add onPressed callback
                  },
                ),
              ],
            ),
            const SizedBox(
              height: 15.0, // Or width: 10.0 if you want a horizontal SizedBox
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                IconButtonWithLabel(
                  icon: Icons.thumb_up,
                  label: 'ตรวจรางวัล',
                  onPressed: () {
                    // Add onPressed callback
                    print('ตรวจรางวัล');
                  },
                ),
                IconButtonWithLabel(
                  icon: Icons.thumb_up,
                  label: 'ชำระทรัพย์NPA',
                  onPressed: () {
                    // Add onPressed callback
                    print('ชำระทรัพย์NPA');
                  },
                ),
                IconButtonWithLabel(
                  icon: Icons.thumb_up,
                  label: 'จ่ายบิล',
                  onPressed: () {
                    print('จ่ายบิล');
                  },
                ),
                IconButtonWithLabel(
                  icon: Icons.thumb_up,
                  label: 'ชำระค่าบริการ',
                  onPressed: () {
                    print('ชำระค่าบริการ');
                  },
                ),
              ],
            ),
            const SizedBox(
              height: 30.0,
            ),
          ],
        ),
      ),
    );
  }
}

class IconButtonWithLabel extends StatelessWidget {
  final IconData icon;
  final String label;
  final VoidCallback onPressed;
  final Color buttonColor;
  final double buttonSize;
  final Color iconColor;
  final double iconSize;

  const IconButtonWithLabel({
    Key? key,
    required this.icon,
    required this.label,
    required this.onPressed,
    this.buttonColor = const Color.fromARGB(255, 248, 100, 2),
    this.buttonSize = 48.0,
    this.iconColor = const Color.fromARGB(255, 255, 255, 255),
    this.iconSize = 22.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: buttonSize,
          height: buttonSize,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: buttonColor,
          ),
          child: IconButton(
            icon: Icon(
              icon,
              color: iconColor,
              size: iconSize,
            ),
            color: buttonColor,
            onPressed: onPressed,
          ),
        ),
        Text(
          label,
          style: const TextStyle(fontSize: 12),
        ),
      ],
    );
  }
}

class TextSection extends StatelessWidget {
  const TextSection({
    super.key,
    required this.description,
  });

  final String description;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(32),
      child: Text(
        description,
        softWrap: true,
      ),
    );
  }
}

class ImageSection extends StatelessWidget {
  const ImageSection({super.key, required this.image});

  final String image;

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      image,
      width: 600,
      height: 240,
      fit: BoxFit.cover,
    );
  }
}

class _DemoBottomAppBar extends StatelessWidget {
  const _DemoBottomAppBar({
    this.fabLocation = FloatingActionButtonLocation.endDocked,
    this.shape = const CircularNotchedRectangle(),
  });

  final FloatingActionButtonLocation fabLocation;
  final NotchedShape? shape;

  static final List<FloatingActionButtonLocation> centerLocations =
      <FloatingActionButtonLocation>[
    FloatingActionButtonLocation.centerDocked,
    FloatingActionButtonLocation.centerFloat,
  ];

  @override
  Widget build(BuildContext context) {
    final Color color = Theme.of(context).cardColor;
    return BottomAppBar(
      shape: shape,
      color: const Color.fromARGB(255, 255, 81, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [

          GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const WelcomeScreen()),
                );
              },
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.star,
                      color: const Color.fromARGB(255, 255, 255, 255)),
                  Padding(
                    padding: const EdgeInsets.only(top: 8),
                    child: Text(
                      "บริการ",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: color,
                      ),
                    ),
                  ),
                ],
              )),
          GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const AccountScreen()),
                );
              },
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.account_balance_rounded,
                      color: const Color.fromARGB(255, 255, 255, 255)),
                  Padding(
                    padding: const EdgeInsets.only(top: 8),
                    child: Text(
                      "บัญชี",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: color,
                      ),
                    ),
                  ),
                ],
              )),
          GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const NotifyScreen()),
                );
              },
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.circle_notifications_outlined,
                      color: const Color.fromARGB(255, 255, 255, 255)),
                  Padding(
                    padding: const EdgeInsets.only(top: 8),
                    child: Text(
                      "แจ้งเตือน",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: color,
                      ),
                    ),
                  ),
                ],
              )),
          GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const SettingScreen()),
                );
              },
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.settings,
                      color: const Color.fromARGB(255, 255, 255, 255)),
                  Padding(
                    padding: const EdgeInsets.only(top: 8),
                    child: Text(
                      "ตั้งค่า",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: color,
                      ),
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }
}

class BottomAppBarDemo extends StatefulWidget {
  const BottomAppBarDemo({super.key});

  @override
  State createState() => _BottomAppBarDemoState();
}

class _BottomAppBarDemoState extends State<BottomAppBarDemo> {
  bool _showFab = true;
  bool _showNotch = true;
  FloatingActionButtonLocation _fabLocation =
      FloatingActionButtonLocation.endDocked;

  void _onShowNotchChanged(bool value) {
    setState(() {
      _showNotch = value;
    });
  }

  void _onShowFabChanged(bool value) {
    setState(() {
      _showFab = value;
    });
  }

  void _onFabLocationChanged(FloatingActionButtonLocation? value) {
    setState(() {
      _fabLocation = value ?? FloatingActionButtonLocation.endDocked;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: const Text('Bottom App Bar Demo'),
        ),
        body: ListView(
          padding: const EdgeInsets.only(bottom: 88),
          children: <Widget>[
            SwitchListTile(
              title: const Text(
                'Floating Action Button',
              ),
              value: _showFab,
              onChanged: _onShowFabChanged,
            ),
            SwitchListTile(
              title: const Text('Notch'),
              value: _showNotch,
              onChanged: _onShowNotchChanged,
            ),
            const Padding(
              padding: EdgeInsets.all(16),
              child: Text('Floating action button position'),
            ),
            RadioListTile<FloatingActionButtonLocation>(
              title: const Text('Docked - End'),
              value: FloatingActionButtonLocation.endDocked,
              groupValue: _fabLocation,
              onChanged: _onFabLocationChanged,
            ),
            RadioListTile<FloatingActionButtonLocation>(
              title: const Text('Docked - Center'),
              value: FloatingActionButtonLocation.centerDocked,
              groupValue: _fabLocation,
              onChanged: _onFabLocationChanged,
            ),
            RadioListTile<FloatingActionButtonLocation>(
              title: const Text('Floating - End'),
              value: FloatingActionButtonLocation.endFloat,
              groupValue: _fabLocation,
              onChanged: _onFabLocationChanged,
            ),
            RadioListTile<FloatingActionButtonLocation>(
              title: const Text('Floating - Center'),
              value: FloatingActionButtonLocation.centerFloat,
              groupValue: _fabLocation,
              onChanged: _onFabLocationChanged,
            ),
          ],
        ),
        floatingActionButton: _showFab
            ? FloatingActionButton(
                onPressed: () {},
                tooltip: 'Create',
                child: const Icon(Icons.add),
              )
            : null,
        floatingActionButtonLocation: _fabLocation,
        bottomNavigationBar: _DemoBottomAppBar(
          fabLocation: _fabLocation,
          shape: _showNotch ? const CircularNotchedRectangle() : null,
        ),
      ),
    );
  }
}
