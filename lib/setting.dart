import 'package:flutter/material.dart';

class SettingScreen extends StatelessWidget {
  const SettingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              // Handle back button press
              Navigator.pop(context);
            },
          ),
          title: Text('แจ้งเตือน'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
              },
            ),
          ],
        ),
        body: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image:
                  AssetImage('images/lake.jpg'), 
              fit: BoxFit.cover, 
            ),
          ),
          alignment: Alignment.topCenter, 
          padding: EdgeInsets.all(16.0), 
          child: Column(
            children: [
              Card(
                elevation: 4.0,
                child: Container(
                  constraints: BoxConstraints(maxHeight: 65),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('ตั้งค่าบัญชี'),
                            TextButton(
                              onPressed: () {},
                              child: Text(
                                '>',
                                style: TextStyle(
                                  color: Color.fromARGB(255, 255, 72, 0),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
                            Card(
                elevation: 4.0,
                child: Container(
                  constraints: BoxConstraints(maxHeight: 65),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('ตั้งค่าสินเชื่อ'),
                            TextButton(
                              onPressed: () {},
                              child: Text(
                                '>',
                                style: TextStyle(
                                  color: Color.fromARGB(255, 255, 72, 0),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),

               Card(
                elevation: 4.0,
                child: Container(
                  constraints: BoxConstraints(maxHeight: 65),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('ตั้งค่าข้อมูลส่วนบุคคล'),
                            TextButton(
                              onPressed: () {},
                              child: Text(
                                '>',
                                style: TextStyle(
                                  color: Color.fromARGB(255, 255, 72, 0),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
               Card(
                elevation: 4.0,
                child: Container(
                  constraints: BoxConstraints(maxHeight: 65),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('ตั้งค่าการแจ้งเตือน'),
                            TextButton(
                              onPressed: () {},
                              child: Text(
                                '>',
                                style: TextStyle(
                                  color: Color.fromARGB(255, 255, 72, 0),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}